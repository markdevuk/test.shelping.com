<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('login');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => '/projects',  'middleware' => 'web'], function() {
    Route::get('create', 'ProjectController@newProject')->name('new-project');
    Route::get('view/{token}', 'ProjectController@viewProject')->name('view-project');
    Route::post('save', 'ProjectController@saveProject')->name('save-project');

    Route::get('image/{image}', function ($image)
    {
        $file = null;
        if(!empty($image)) {

            $projectImage = Storage::disk('project')->exists('images/' . $image);
            if(!empty($projectImage)) {
                $path = Storage::disk('project')->path('images/' . $image);

                $file = File::get($path);
                $type = File::mimeType($path);

                $response = Response::make($file, 200);
                $response->header("Content-Type", $type);
                return $response;
            }

        } else {
            abort(404);
        }



    })->name('image-project');
});

Route::group(['prefix' => '/',  'middleware' => 'web'], function() {
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
});
