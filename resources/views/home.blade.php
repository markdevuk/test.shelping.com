@extends('layouts.auth')

@section('content')
    <div class="row col-md-12">
        @include('layouts.parts.sidebar')
        <div class="col-md-8 no-gutters" id="projects">
            <h1>Random 6 Projects</h1>
            @if($projects->count())
                @foreach($projects AS $project)
                    <div class="col-md-5 d-inline-block mb-2">
                        <div class="card">
                            <div class="card-header"><span></span>{!! $project->name !!} <small class="pull-right created-at"><i>{!! date('d/m/Y H:i:s', strtotime($project->created_at)) !!}</i></small></div>

                            <div class="card-body text-center">

                                <div class="img-overflow">
                                    <img src="{!! route('image-project', $project->image) !!}" />
                                </div>
                                <p>{!! substr($project->description, 0, 50) !!}</p>
                                <a class="btn btn-lg btn-success" onclick="loader('show')" href="{!! route('view-project', $project->token) !!}">VIEW PROJECT</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <p>There are no projects saved.</p>
            @endif
        </div>
    </div>
@endsection