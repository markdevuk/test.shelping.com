@extends('layouts.noauth')

@section('content')
    <div class="col col-md-5 pull-left">
        <h1>Demo Test Outline</h1>
        <ul>
            <li>Create project in Laravel/foundation.</li>
            <li>Have a very simple MySQL DB setup which will have two tables – users and projects</li>
            <li>Users: Id, name, email, mobile, last_project_viewed, password</li>
            <li>Projects: Id, user_id, name, desc, image</li>
            <li>Have a signup/signin form for users with validation.</li>
            <li>Have a project upload page (only available for logged in users) again with validation.</li>
            <li>Have a projects page – randomly display 6 projects. Have a project search where projects can be searched for by name and also by desc (just done through a single textbox).</li>
            <li>Have the search box show suggestions as you type (like google search text box)</li>
            <li>Perform search, update displayed projects.</li>
            <li>Have the DB on a ‘backend’ with an API so that the ‘frontend’ can make an API call passing parameters and receiving them back for use.</li>
            <li>All pages need to be responsive in design</li>
            <li>Add project to GIT so there is a repo in place.</li>
            <li>Demonstrate some AJAX work</li>
            <li>All forms need validation</li>
        </ul>
        <p>Please note: For the purposes of this test</p>
        <ul>
            <li>I will be using Bootstrap 4 instead of Foundation</li>
            <li>I will use jQuery AJAX on the auto suggest search field</li>
            <li>I will use an API call on the auto suggest field</li>
            <li>Results passed back will be in JSON format</li>
        </ul>
        <p>
            Test developed by Mark Wright
            <br>
            Time taken: 7.5 hours
        </p>
    </div>
    <div class="col col-md-6 pull-right">
        <div class="card">
            <div class="card-header">{{ __('Login') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('login') }}" onsubmit="return loader('show')">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>


                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>
@endsection
@section('scripts')
    <script src="{!! asset('/js/auth/functions.js') !!}" ></script>
@endsection