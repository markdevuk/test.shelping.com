<div class="col-md-4">
    <div class="card">
        <div class="card-header">NEW PROJECT</div>

        <div class="card-body">
            <p>Create a new project.</p>
            <a  onclick="loader('show')" class="btn btn-lg btn-primary" onclick="return loader('show')" href="{!! route('new-project') !!}">CREATE PROJECT</a>
        </div>
    </div>
    <div class="card mt-2" id="last-viewed">
        <div class="card-header">LAST PROJECT VIEWED</div>

        <div class="card-body">
            @if(!empty($lastprojectviewed))
                <div class="col-md-4 pull-left">
                    <img src="{!! route('image-project', $lastprojectviewed->image) !!}" />
                </div>
            <p>
                {!! $lastprojectviewed->name !!}
            </p>
                <a  onclick="loader('show')" class="btn btn-lg btn-primary" onclick="return loader('show')" href="{!! route('view-project', $lastprojectviewed->token) !!}">VIEW PROJECT</a>
            @else
                <p>No projects have been viewed.</p>
            @endif
        </div>
    </div>
    <div class="card mt-2 mb-2" id="find-project">
        <div class="card-header">SEARCH PROJECTS</div>

        <div class="card-body">
            <p>Find project using the search bar below. The results will be highlighted.</p>
            <input id="search" class="form-control"/>
            <div id="search-results"></div>
        </div>
    </div>
</div>