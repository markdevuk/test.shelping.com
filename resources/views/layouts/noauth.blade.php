<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>{!! config('app.name', 'Shelping') !!}</title>
</head>
<body>
    <div id="shelping">
        <nav class="navbar row col-md-12 no-gutters">
            <ul class="col-md-12 no-gutters">
                <li class="col-md-4 d-inline-block">
                    @if(!empty($user))
                        <a href="{!! route('home') !!}"><img src="http://www.shelping.com/assets/images/shelping-logo.png"/></a>
                    @else
                        <a href="{!! route('login') !!}"><img src="http://www.shelping.com/assets/images/shelping-logo.png"/></a>
                    @endif
                </li>

                <li class="col-md-7 d-inline-block text-right noauth-menu">
                    @if(!empty($user))
                        <p>Welcome {!! $user->name !!} |
                            <a href="{!! route('logout') !!}">LOGOUT</a>
                        </p>
                    @else
                        <a href="{!! route('register') !!}">REGISTER</a> |
                        <a href="{!! route('login') !!}">LOGIN</a>
                    @endif
                </li>
            </ul>
        </nav>

        <main class="col-md-12">
            @yield('content')
        </main>
    </div>
    <div id="loader">
        <div id="overlay"></div>
        <div id="information">
            <img src="{!! asset('/img/loading.gif') !!}" />
            <p>One moment please...</p>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="{!! asset('/js/app.js') !!}" ></script>
    @yield('scripts')


</body>
</html>
