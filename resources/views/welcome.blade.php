@extends('layouts.noauth')

@section('content')
    <div class="content">
        <div id="test-outline" class="col-md-12 text-center mt-20">
            <h1>Demo Task</h1>
            <ul>
                <li>Create project in Laravel/foundation.</li>

                <li>Have a very simple MySQL DB setup which will have two tables – users and projects</li>

                <li>Users: Id, name, email, mobile, last_project_viewed, password</li>

                <li>Projects: Id, user_id, name, desc, image</li>

                <li>Have a signup/signin form for users with validation.</li>

                <li>Have a project upload page (only available for logged in users) again with validation.</li>

                <li>Have a projects page – randomly display 6 projects. Have a project search where projects can be searched for by name and also by desc (just done through a single textbox).</li>

                <li>Have the search box show suggestions as you type (like google search text box)</li>

                <li>Perform search, update displayed projects.</li>

                <li>Have the DB on a ‘backend’ with an API so that the ‘frontend’ can make an API call passing parameters and receiving them back for use.</li>

                <li>All pages need to be responsive in design</li>

                <li>Add project to GIT so there is a repo in place.</li>

                <li>Demonstrate some AJAX work</li>

                <li>All forms need validation</li>
            </ul>
            <p>For the purposes of this test, I will be using Bootstrap 4 instead of Foundation.</p>
            <p>Test developed by Mark Wright</p>
        </div>
    </div>
@endsection
