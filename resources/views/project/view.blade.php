@extends('layouts.auth')

@section('content')
    <div class="row col-md-12">
        @include('layouts.parts.sidebar')
        <div class="col-md-8 no-gutters" id="view-projects">
            @if($project)
                <h1 class="text-left">Project: {!! $project->name !!} <small class="pull-right"><i>Added: {!! date('d/m/Y H:i:s', strtotime($project->created_at)) !!}</i></small></h1>
                <a href="/home" class="btn btn-md btn-light">BACK</a>
                <div class="col-md-12 mt-2 text-center" id="banner">
                    <img src="{!! route('image-project', $project->image) !!}" />
                </div>
                <div class="col-md-12 text-center mt-2">
                    <p>{!! $project->description !!}</p>
                </div>
            @else
                <h1>Whoops!</h1>
                <p>That project does not exist.</p>
            @endif
        </div>
    </div>
@endsection