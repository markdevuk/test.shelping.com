@extends('layouts.auth')

@section('content')
    <div class="row col-md-12">
        @if(session('status'))
            <div id="status" class="alert alert-{!! session('status')['state'] !!} col-md-12">
                {!! session('status')['message'] !!}
            </div>
        @endif
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">NEW PROJECT</div>

                <div class="card-body">
                    <p>All fields are required.</p>
                    <form method="POST" action = "{!! route('save-project') !!}" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label">{{ __('Name') }}</label>

                            <div class="col-md-10">
                                <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label">{{ __('Description') }}</label>
                            <div class="col-md-10">
                                <textarea id="description" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="description" required></textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-2 col-form-label">{{ __('Project Image') }}</label>

                            <div class="col-md-10">
                                <input type="file" id="image" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" required/>
                                @if ($errors->has('file'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="form-group row">
                            <label for="submit" class="col-md-2 col-form-label">&nbsp;</label>

                            <div class="col-md-10">
                                <a onclick="loader('show')" href="/home" class="btn btn-lg btn-light">BACK</a>
                                <button onclick="loader('show')" type="submit" class="btn btn-lg btn-primary">CREATE PROJECT</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">EXAMPLE PROJECT</div>
                <div class="card-body">
                    <h2>Youth Football Teams</h2>
                    <p>Let's take the example of a youth football team - Youth United under 10s. For the sake of example we'll assume the following:</p>
                    <ul>
                        <li>There are 12 kids associated with Youth United Under 10s meaning there are 12 families or households involved.</li>
                        <li>Each family (so the mums and dads) get two of their relatives to buy their everyday products through Shelping.COM so that's an additional 24 households buying their products to create donations towards Youth United Under 10s.</li>
                        <li>Each family also get two of their friends to also buy their everyday products through Shelping.COM so that's a further 24 households buying their products to create donations towards Youth United Under 10s - and bear in mind they are only buying what they already buy anyway.</li>
                        <li>All in all that's 60 households buying their everyday products from Shelping.COM with donations going towards Youth United Under 10s.</li>
                        <li>Each household spends just &pound;30.00/month on their everyday items through Shelping.COM - that's less than &pound;7.50 each week.</li>
                        <li>Each household creates &pound;3.75 per month as donations towards Youth United Under 10s.</li>
                        <li>That makes a total of 60 lots of &pound;3.75 each month, which is &pound;225.00 every month.</li>
                        <li>Better than that, though, it makes a total of <strong>&pound;2,700.00 every year</strong>.</li>
                    </ul>
                    <p>Think what your child's football team could do with<strong> &pound;2,700 every year</strong>... new kits... new goals... new nets... new balls... summer tour to Holland for a tournament... end of season awards with food and drinks laid on... whatever they want!</p>
                    <p>Now think what could actually happen if all the teams in all the age groups for Youth United joined together. That really could make a difference to Youth United and grass roots football generally. Now that's Shelping.COM!</p>
                </div>
            </div>
        </div>
    </div>
@endsection
