let apiToken = $('meta[name="api-token"]').attr('content');
$(function(){
    $.ajaxSetup({
        headers: {
            'Authorization':'Bearer ' + apiToken,
        },
        dataType: 'JSON'
    });

    $('#find-project').on('keyup', '#search', function(){
        let $this = $(this);
        let val = $this.val();
        let resultsContainer = $('#search-results');

        if(val.length > 0) {
            $.ajax({
                url: '/api/projects/search',
                type: 'get',
                data: {
                    search : val
                }
            }).done(function(json){
                if(json.status === "ok") {
                    let projects = json.projects;

                    let projectsCount = projects.length;
                    if(projectsCount > 0) {
                        let html = "<ul>";
                        for (let i = 0; i < projectsCount; i++) {
                            html += "<li class='pull-left col-md-12'>";
                            html += "<div class='img-overflow col-md-3 pull-left'>";
                            html += "<img src='/projects/image/" + projects[i].image + "' />";
                            html += "</div>";
                            html += "<p class='col-md-9 pull-left'><strong>" + projects[i].name + "</strong><br>";
                            html += projects[i].description;
                            html += "</p><br><a href='/projects/view/" + projects[i].token + "' class='btn btn-sm btn-primary pull-right'>VIEW PROJECT</a>";
                            html += "</li>";
                        }
                        html += "</ul>";
                        resultsContainer.html(html).slideDown();
                    } else {
                        resultsContainer.html('<p class="mt-2 alert alert-warning">There are no results with that criteria</p>').slideDown();
                    }

                }

            })
        } else {
            resultsContainer.html('').slideUp();
        }

    })
})

function loader(action) {
    let loader = $('#loader');
    if(action === "show")
        loader.show();
    else
        loader.hide();
}