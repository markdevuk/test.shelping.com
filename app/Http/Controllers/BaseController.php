<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use View;

class BaseController extends Controller
{
    protected $inputs;
    protected $user;
    protected $helper;

    public function __construct(Request $request)
    {
        $this->helper = new Helper();
        $inputs = $request->all();
        $this->inputs = $this->helper->sanitiseInputs($inputs);
    }



}