<?php

namespace App\Http\Controllers;

use \App\Project;
use View;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome()
    {
        $user = auth()->user();

        return View::make('welcome')->withUser($user);
    }
    public function index()
    {
        $user = auth()->user();
        $lastProjectViewed = $user->getLastProjectViewed()->first();
        $projects = Project::limit(6)->inRandomOrder()->get();
        return View::make('home')->withUser($user)->withProjects($projects)->withLastprojectviewed($lastProjectViewed);
    }
}
