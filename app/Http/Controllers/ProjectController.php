<?php

namespace App\Http\Controllers;

use Auth;
use \App\Project;
use Illuminate\Http\Request;
use Storage;
use View;

class ProjectController extends BaseController
{

    public function viewProject($token)
    {
        if(Auth::user()) {
            $lastProjectViewed = null;
            $user = auth()->user();
            $token = $this->helper->sanitiseInputs(array('token' => $token))['token'];
            $project = Project::where('token', $token)->first();
            if(!empty($project)) {
                $lastProjectViewed = $project;
                $user->last_project_viewed = $project->id;
                $user->save();
            }
            return View::make('project.view')->withUser($user)->withProject($project)->withLastprojectviewed($lastProjectViewed);
        } else {
            return redirect()->to('/logout');
        }
    }

    public function newProject()
    {
        if(Auth::user()) {
            $user = auth()->user();
            return View::make('project.new')->withUser($user);
        } else {
            return redirect()->to('/logout');
        }
    }

    public function saveProject(Request $request)
    {
        if(Auth::user()) {
            $this->validate($request, [

                'name' => ['required', 'string', 'max:255'],
                'description' => ['required', 'string'],
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

            ]);

            $user = auth()->user();
            $name = $this->inputs['name'];
            $description = $this->inputs['description'];
            $image = $request->file('file');



            $project = new Project();
            $project->name = $name;
            $project->user_id = $user->id;
            $project->description = $description;

            $project->save();
            $token = md5(time() . $project->id);
            $newFile = $token . time() . '.' . $image->getClientOriginalExtension();

            Storage::disk('project')->put('images/' . $newFile, file_get_contents($image));

            $project->token = $token;
            $project->image = $newFile;
            $project->save();

            return redirect()->back()->with(['status' => ['message' => 'Project uploaded successfully. Add another project or <a href="/home" class="btn btn-sm btn-primary">GO BACK</a>', 'state' => 'success']]);
        } else {
            return redirect()->to('/logout');
        }
    }

}
