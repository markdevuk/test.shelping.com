<?php

namespace App\Http\Controllers;

use Auth;
use \App\Project;
use View;

class ApiController extends BaseController
{

    public function search()
    {
        if(Auth::user()) {
            $search = $this->inputs['search'];
            $projectArr = array();
            $projects = Project::where('name', 'LIKE', '%' . $search . '%')->orWhere('description', 'LIKE', '%' . $search . '%')->get();
            //Lets try to highlight the text
            foreach($projects AS $key => $project) {
                //Lets highlight the name first
                $textName = str_replace(strtolower($search), '<strong>' . ucwords($search) . '</strong>', strtolower($project->name));
                $textDescription = str_replace(strtolower($search), '<strong>' . ucwords($search) . '</strong>', strtolower($project->description));

                //Let's trim the description down
                $startPosition = strpos(strtolower($textDescription), strtolower($search));
                $subDescription = substr($textDescription, ($startPosition - 8), 100) . "...";

                $projectArr[$key]['name'] = ucwords($textName);
                $projectArr[$key]['description'] = $subDescription;
                $projectArr[$key]['image'] = $project->image;
                $projectArr[$key]['token'] = $project->token;
            }
            return json_encode(array('status' => 'ok', 'projects' => $projectArr));
        } else {
            abort(401, 'Permission Denied');
        }

    }

}
