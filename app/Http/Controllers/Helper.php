<?php

namespace App\Http\Controllers;

class Helper
{
    public function sanitiseInputs($inputs)
    {
        if(!empty($inputs)) {
            foreach($inputs AS $key => $input) {
                $sanitised = filter_var($input, FILTER_SANITIZE_STRING);
                $inputs[$key] = $sanitised;
            }
        }
        return $inputs;
    }
}